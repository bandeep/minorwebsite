def validate_response_data(response):
    #print response
    #print response.json()
    #print response.__dict__
    #print response.status_code
    if response.status_code == 200 or response.status_code == 201:
        if (any(response)):
            return response.json()
        else:
            return {"response": "The server has fulfilled the request."}
    elif (response.status_code == 202):
        if (any(response)):
            return response.json()
        else:
            return {"response": "The server has fulfilled the request by reverting / deleting the resource."}
    elif(response.status_code == 204):
        if (any(response)):
            return response.json()
        else:
            return { "response":"The server has fulfilled the request."}
    elif (response.status_code == 400 ):
        return "Error Code : " + str(response.status_code) + ' , ' + response.json()['badRequest']['message']
    elif (response.status_code == 401 ):
        return "Error Code : " + str(response.status_code) + ' , ' + response.json()['unauthorized']['message']
    elif (response.status_code == 403 ):
        if(response.json()['forbidden'] is not None ):
            return "Error Code : " + str(response.status_code) + ' , ' + response.json()['forbidden']['message']
        elif(response.json()['error'] is not None ):
            return "Error Code " + str(response.status_code) + ' , ' + response.json()['error']['message']
    elif (response.status_code == 409 ):
        return "Error Code : " + str(response.status_code) + ' , ' + response.json()['conflict']['message']
    # else:
    #     return "Error Code : "+str(response.status_code)+' , '+response.json()['error']['message']