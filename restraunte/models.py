from __future__ import unicode_literals

from django.db import models
# Create your models here.
# adadadad
class maindata(models.Model):
    Restaurant_ID=models.IntegerField()
    Restaurant_Name=models.CharField(max_length=5000)
    City=models.CharField(max_length=5000)
    Address=models.CharField(max_length=5000)
    Locality_Verbose=models.CharField(max_length=5000)
    Longitude=models.FloatField()
    Latitude=models.FloatField()
    Cuisines=models.CharField(max_length=5000)
    Average_Cost_for_two=models.IntegerField()
    Has_Table_booking=models.IntegerField()
    Has_Online_delivery=models.IntegerField()
    Price_range=models.IntegerField()
    Aggregate_rating=models.FloatField()
    Rating_text=models.CharField(max_length=5000)
    Votes=models.IntegerField()
    Id = models.IntegerField()

    def image_thumbnail(self):
        return '<img src="%s">' % (self.img_brief)

    image_thumbnail.allow_tags = True
    image_thumbnail.short_description = 'Image Breif'